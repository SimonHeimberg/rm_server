from rm_replication.settings.base import *

DEBUG = True

SECRET_KEY = 'my_secret_key'

ALLOWED_HOSTS = ['192.168.1.11', '192.168.1.110', '127.0.0.1', '192.168.165.247', '192.168.105.224', 'air.local']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'remem-me_db',
        'USER': 'remem-me_rm',
        'PASSWORD': 'my_secret',
        'HOST': 'localhost',
        'PORT': '5432',
        'ATOMIC_REQUESTS': True,
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
