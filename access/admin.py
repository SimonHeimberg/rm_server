from cuser.admin import UserAdmin
from django.contrib import admin

from access.models import User

admin.site.register(User, UserAdmin)
