from django.contrib.auth import get_user_model
from rest_framework import serializers
from django.utils.translation import gettext_lazy as _
User = get_user_model()


class UserCreateSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=True, style={
        "input_type": "password"})

    class Meta:
        model = User
        fields = [
            "email",
            "password",
            "ip_address",
        ]
        extra_kwargs = {"password": {"write_only": True}}

    def create(self, validated_data):
        email = validated_data["email"]
        password = validated_data["password"]
        ip = validated_data["ip_address"]
        if email and User.objects.filter(email__iexact=email).exists():
            raise serializers.ValidationError(
                {"email": _("A user with that email address already exists.")})
        user = User(email=email, is_active=False, ip_address=ip)
        user.set_password(password)
        user.save()
        return user
