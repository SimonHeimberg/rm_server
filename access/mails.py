import logging

from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMultiAlternatives
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.utils.text import format_lazy as f
from django.utils.translation import gettext_lazy as _

from access.tokens import account_activation_token

logger = logging.getLogger('app')


def send_activation_mail(request, user):
    subject = _('User Activation')
    domain = get_current_site(request).domain
    uidb64 = urlsafe_base64_encode(force_bytes(user.pk))
    token = account_activation_token.make_token(user)
    lang = request.stream.LANGUAGE_CODE
    url = f'http://{domain}/v1/access/activate/{lang}/{uidb64}/{token}.'

    text_content = f(_("Hello,\nThank you for using Remember Me. Please click on the link to confirm your registration, {url}\n"
             "If you haven't registered, then just ignore this email.\nGod bless,\nRev. Peter Schaffluetzel "), url=url)
    html_content = f('<p>{text_content}<br><a href="https://www.remem.me">www.remem.me</a></p>', text_content=text_content)
    from_email = "Remember Me Support <support@remem.me>"
    msg = EmailMultiAlternatives(subject, text_content, from_email, [user.email, ])
    msg.attach_alternative(html_content, "text/html")
    msg.send()
    logger.info(f'Activation email sent to {user.email}: {text_content}')
