import logging

from django.contrib.auth.tokens import PasswordResetTokenGenerator

from access.models import User
logger = logging.getLogger('app')


class AccountActivationTokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user: User, timestamp):
        login_timestamp = '' if user.last_login is None else user.last_login.replace(microsecond=0, tzinfo=None)
        logger.info(f'Hash value: {user.pk}{user.is_active}{login_timestamp}{timestamp}{user.email}')
        return f'{user.pk}{user.is_active}{login_timestamp}{timestamp}{user.email}'


account_activation_token = AccountActivationTokenGenerator()
