��    
      l      �       �   .   �   :      �   [          9     I     X     o  
   }  B  �  +   �  W   �  �   O     B     a     u  "   �     �  
   �                         
             	       A user with that email address already exists. Activation email sent to {email}. Please check your inbox. Hello,
Thank you for using Remember Me. Please click on the link to confirm your registration, {url}
If you haven't registered, then just ignore this email.
God bless,
Rev. Peter Schaffluetzel  Too many open registrations User Activation User activated User activation failed email address ip address Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Diese Email-Adresse wird bereits verwendet. Email zur Aktivierung an {email} gesendet. Schauen Sie bitte in Ihrem Posteingang nach. Grüss Gott!
Danke, dass Sie Remember Me verwenden. Klicken Sie bitte auf den Link, um die Registrierung abzuschliessen: {url}
Falls Sie sich nicht registriert haben, können Sie diese E-Mail ignorieren.
Gottes Segen!
Pfr. Peter Schafflützel Zuviele offene Registrierungen Benutzeraktivierung Benutzer aktiviert Benutzeraktivierung fehlgeschlagen Email-Adresse IP-Adresse 