import socket

from django.contrib.auth import get_user_model
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from rest_framework import response, decorators, permissions, status
from datetime import date, timedelta
from django.utils.text import format_lazy as f
from django.utils.translation import gettext_lazy as _

from main.models import Account
from .mails import send_activation_mail
from .serializers import UserCreateSerializer
from .tokens import account_activation_token

User = get_user_model()


@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.AllowAny])
def registration(request):
    ip = _valid_ip_address(request)
    if ip is None:
        return response.Response({'messages': [_('No valid ip address')]}, status.HTTP_400_BAD_REQUEST)
    if _too_many_registrations(ip):
        return response.Response({'messages': [_('Too many open registrations')]}, status.HTTP_400_BAD_REQUEST)
    serializer = UserCreateSerializer(data={**request.data, 'ip_address': ip})
    if not serializer.is_valid():
        return response.Response(serializer.errors, status.HTTP_400_BAD_REQUEST)
    user = serializer.save()
    send_activation_mail(request, user)
    message = f(_('Activation email sent to {email}. Please check your inbox.'), email=user.email)
    return response.Response({'messages': [message]}, status.HTTP_201_CREATED)


def _valid_ip_address(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    try:
        socket.inet_aton(ip)
        return ip
    except socket.error:
        return None


def _too_many_registrations(ip):
    return len(User.objects.filter(is_active=False, ip_address=ip, date_joined__gte=date.today() - timedelta(7))) > 2


@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.AllowAny])
def activation(request, lang, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        account_name = user.email.split('@')[0]
        index = 0
        while True:
            count = Account.objects.filter(name=account_name).count()
            if count == 0:
                break
            index += 1
            account_name += str(index)
        Account(user=user, name=account_name, language=lang,).save()
        return response.Response({'messages': [_('User activated')]}, status.HTTP_202_ACCEPTED)
    else:
        return response.Response({'messages': [_('User activation failed')]}, status.HTTP_406_NOT_ACCEPTABLE)
