from django.urls import path, re_path
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from .views import registration, activation

app_name = 'access'
urlpatterns = [
    path('register/', registration, name='registration'),
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    re_path(
        '^activate/(?P<lang>[A-Za-z_-]+?)/(?P<uidb64>[0-9A-Za-z_-]+)/(?P<token>[0-9A-Za-z]{1,20}-[0-9A-Za-z]{1,40})/$',
        activation,
        name='activation'),
]
