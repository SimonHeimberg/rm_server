from django.db import models
from django.utils.translation import ugettext_lazy as _
from cuser.models import AbstractCUser, CUserManager, CUser


class CaseInsensitiveUserManager(CUserManager):
    def get_by_natural_key(self, username):
        return self.get(**{self.model.USERNAME_FIELD + '__iexact': username})


class User(AbstractCUser):
    objects = CaseInsensitiveUserManager()
    ip_address = models.CharField(_('ip address'), max_length=40)

