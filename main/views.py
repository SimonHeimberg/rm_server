from django.db.models import Count, F
from rest_framework import viewsets, permissions, generics
from rest_framework.exceptions import PermissionDenied
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.permissions import AllowAny

from .models import Account, Verse, Tag, Score, Deck
from .serializers import AccountSerializer, TagSerializer, VerseSerializer, ScoreSerializer, CollectionSerializer, \
    CollectionVerseSerializer, DeckSerializer


class IsOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        try:
            return obj.user == request.user
        except AttributeError:
            try:
                return obj.account.user == request.user
            except AttributeError:
                return obj.tag.account.user == request.user


class AccountViewSet(viewsets.ModelViewSet):
    serializer_class = AccountSerializer
    permission_classes = [IsOwner]

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            return Account.objects.filter(user=user)
        raise PermissionDenied()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class AccountableViewSet(viewsets.ModelViewSet):
    permission_classes = [IsOwner]
    model_class = None

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            query = self.model_class.objects.filter(account__user=user, deleted=False)
            account = self.request.query_params.get('account')
            if account:
                query = query.filter(account__id=account)
            return query
        raise PermissionDenied()


class TagViewSet(AccountableViewSet):
    model_class = Tag
    serializer_class = TagSerializer


class VerseViewSet(AccountableViewSet):
    model_class = Verse
    serializer_class = VerseSerializer


class ScoreViewSet(AccountableViewSet):
    model_class = Score
    serializer_class = ScoreSerializer


class DeckViewSet(viewsets.ModelViewSet):
    permission_classes = [IsOwner]
    model_class = Deck
    serializer_class = DeckSerializer

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            return Deck.objects.filter(tag__account__user=user)
        raise PermissionDenied()


class CollectionList(generics.ListAPIView):
    permission_classes = [AllowAny]
    serializer_class = CollectionSerializer
    pagination_class = LimitOffsetPagination
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['name', 'description', 'tag__account__name']
    ordering_fields = ['name', 'created', 'downloads', 'size', 'featured']
    ordering = ['-featured', '-downloads']

    def get_queryset(self):
        """
        Filtered and paged list of published decks
        """
        queryset = Deck.objects.annotate(size=Count('tag__verses', distinct=True), downloads=Count('importers', distinct=True))
        language = self.request.query_params.get('language', None)
        if language is not None:
            queryset = queryset.filter(tag__account__language__startswith=language)
        return queryset


class CollectionVerseList(generics.ListAPIView):
    permission_classes = [AllowAny]
    serializer_class = CollectionVerseSerializer

    def get_queryset(self):
        """
        Filtered and paged list of published decks
        """
        tag_id = self.kwargs['pk']
        return Tag.objects.get(id=tag_id).verses.all()
