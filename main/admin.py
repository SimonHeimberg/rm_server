from django.contrib import admin

from main.models import *

# Register your models here.

@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    search_fields = ['name']
    autocomplete_fields = ['user']


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    search_fields = ['id', 'account__name', 'text', 'deck__key']
    autocomplete_fields = ['account']


@admin.register(Deck)
class DeckAdmin(admin.ModelAdmin):
    search_fields = ['name', 'key', 'tag__account__name']
    autocomplete_fields = ['tag']
    filter_horizontal = ('importers',)


@admin.register(Verse)
class VerseAdmin(admin.ModelAdmin):
    search_fields = ['reference']
    autocomplete_fields = ['tags', 'account']
    filter_horizontal = ('tags',)


@admin.register(Score)
class ScoreAdmin(admin.ModelAdmin):
    search_fields = ['account_name']
    autocomplete_fields = ['account']
