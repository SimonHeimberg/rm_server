from datetime import date, timedelta

from django.db import models
from django.utils import timezone

from access.models import User
from main.util import now, IdGenerator
from django.utils.translation import ugettext_lazy as _



# Future data model:
#
#                        Verse
#                       /  |
#                      1:n |
#                     /    |
# User----1:n----Account    n:m
# (Django auth)       \    |
#                      1:n |
#                       \  |
#                        Tag----1:?----Deck (language, lang_ref defined by Account)
#                -language (passage, tts, search)
#                -lang_ref (reference, tts, sorting, Bible query)
#                -schedule type: exponential, day->week->month (fixed/no_progression set in 'frequency', e.g. prayers)


class Accountable(models.Model):
    account = models.ForeignKey('Account', on_delete=models.CASCADE)

    class Meta:
        abstract = True


class Replicable(models.Model):
    id = models.BigIntegerField(default=IdGenerator.next, primary_key=True)
    modified = models.BigIntegerField(default=now)  # for synchronization
    modified_on_server = models.BigIntegerField(default=now)  # for synchronization
    modified_by = models.CharField(max_length=36, null=True, blank=True)  # for synchronization
    deleted = models.BooleanField(default=False)  # for synchronization and wastebin

    class Meta:
        abstract = True


class Tag(Replicable, Accountable):
    text = models.CharField(max_length=200)
    included = models.BooleanField(null=True)

    @property
    def published(self) -> bool:
        return Deck.objects.filter(tag__id=self.id).exists()

    def __str__(self):
        return '%s (%s)' % (self.text, self.account)

    class Meta:
        constraints = [
            models.CheckConstraint(check=~models.Q(text=""), name="non_empty_text")
        ]
        indexes = [
            models.Index(fields=['account', 'text'], name='tag_text_idx'),
        ]
        ordering = ['text', 'account__name']


class Deck(models.Model):
    tag = models.OneToOneField('Tag', primary_key=True, on_delete=models.CASCADE, related_name='deck')
    importers = models.ManyToManyField('Account', related_name='imported_decks', blank=True)
    name = models.CharField(max_length=500)
    description = models.CharField(max_length=5000)
    created = models.DateTimeField(default=timezone.now)  # allows modification; see doc for 'auto_now_add'
    website = models.CharField(max_length=500, null=True, blank=True)
    image = models.CharField(max_length=500, null=True, blank=True)
    featured = models.BooleanField(default=False)

    @property
    def id(self):
        return self.tag.id

    @property
    def publisher(self):
        return self.tag.account

    @property
    def label(self):
        return self.tag.text

    def __str__(self):
        return '%s (%s, %s)' % (self.name, self.tag.text, self.tag.account)

    class Meta:
        constraints = [
            models.CheckConstraint(check=~models.Q(name=""), name="non_empty_name")
        ]
        ordering = ['name', 'tag__account__name']


class Account(Replicable):
    name = models.CharField(_('name'), max_length=200, unique=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='accounts', null=True, blank=True)
    language = models.CharField(_('language'), max_length=5, default='en')  # verse language
    lang_ref = models.CharField(_('reference language'), max_length=5, null=True, blank=True)  # reference language
    review_frequency = models.FloatField(_('review frequency'), default=2.0)
    review_limit = models.IntegerField(_('review limit'), default=100)
    daily_goal = models.IntegerField(_('daily goal'), default=0)
    last_score_purge = models.DateField(null=True, blank=True)

    # imported_decks defined in Deck
    # verses defined in Verse
    # tags defined in Tag

    @property
    def own_decks(self):
        return Deck.objects.filter(tag__account=self)

    def __str__(self):
        return '%s' % self.name

    class Meta:
        verbose_name = _('account')
        verbose_name_plural = _('accounts')
        db_table = 'main_account'
        indexes = [
            models.Index(fields=['name'], name='account_name_idx'),
        ]
        ordering = ['name']


class Verse(Replicable, Accountable):
    tags = models.ManyToManyField(Tag, related_name='verses', blank=True)
    reference = models.CharField(max_length=250)
    source = models.CharField(max_length=250, null=True, blank=True)
    topic = models.CharField(max_length=500, null=True, blank=True)
    passage = models.CharField(max_length=100000)
    review = models.DateField(null=True, blank=True)
    commit = models.DateField(null=True, blank=True)
    level = models.IntegerField(default=-1)
    image = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return '%s / %s' % (self.reference, self.account)

    class Meta:
        constraints = [
            models.CheckConstraint(check=~models.Q(reference=""), name="non_empty_reference"),
            models.CheckConstraint(check=~models.Q(passage=""), name="non_empty_passage")
        ]
        indexes = [
            models.Index(fields=['account', 'modified_on_server'], name='verse_modified_idx'),
            models.Index(fields=['account', 'deleted', 'reference', 'source'], name='verse_deleted_idx'),
        ]
        ordering = ['reference', 'account__name']


class Score(Replicable, Accountable):
    date = models.DateField()
    vkey = models.CharField(max_length=500)  # reference + source
    change = models.IntegerField()

    def __str__(self):
        return '%s / %s (%s, %s)' % (self.date, self.vkey, self.change, self.account)

    @staticmethod
    def purge(account):
        """Delete all ScoreOps except last 9 days + highscore days
        """
        date_limit = date.today() - timedelta(days=9)
        highscore_dates = Score.highscore_dates(account)
        query = Score.objects.filter(account=account).filter(date__lte=date_limit)
        for dat in highscore_dates:
            query = query.exclude(date=dat)
        query.delete()

    @staticmethod
    def scores(account):
        score_dict = {}
        ops = Score.objects.filter(account=account)
        for op in ops:
            if op.date not in score_dict.keys():
                score_dict[op.date] = op.change
            else:
                score_dict[op.date] = score_dict[op.date] + op.change
        return score_dict

    @staticmethod
    def highscores(account):
        return sorted(Score.scores(account).items(), key=lambda x: -x[1])[:7]

    @staticmethod
    def highscore_dates(account):
        return [x[0] for x in Score.highscores(account)]

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['account', 'date', 'vkey'], name='unique_score'),
        ]
        indexes = [
            models.Index(fields=['account', 'modified_on_server'], name='score_modified_on_server_idx'),
            models.Index(fields=['account', 'modified'], name='score_modified_idx'),
        ]
        ordering = ['-date', 'vkey']

