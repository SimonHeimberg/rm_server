from rest_framework import serializers
from .models import Account, Tag, Verse, Score, Deck


class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'modified', 'modified_by', 'deleted', 'name', 'language', 'lang_ref', 'review_limit',
                  'daily_goal', 'review_frequency')
        model = Account


class TagSerializer(serializers.ModelSerializer):
    published = serializers.BooleanField(read_only=True)

    class Meta:
        fields = ('id', 'modified', 'modified_by', 'deleted', 'account', 'text', 'included', 'published')
        model = Tag


class VerseSerializer(serializers.ModelSerializer):
    class Meta:
        fields = (
            'id', 'modified', 'modified_by', 'deleted', 'account', 'tags', 'reference', 'source', 'topic', 'passage',
            'review', 'commit', 'level',
            'image')
        model = Verse


class ScoreSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'modified', 'modified_by', 'deleted', 'account', 'date', 'vkey', 'change')
        model = Score


class DeckSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('tag', 'name', 'description', 'website', 'image')
        model = Deck


class PublisherSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'name', 'language', 'lang_ref')
        model = Account


class CollectionSerializer(serializers.ModelSerializer):
    created = serializers.DateTimeField(format='%Y-%m-%d')
    publisher = PublisherSerializer()
    downloads = serializers.IntegerField(read_only=True)
    size = serializers.IntegerField(read_only=True)

    class Meta:
        fields = (
            'id', 'label', 'name', 'description', 'website', 'created', 'featured', 'size', 'downloads', 'publisher')
        model = Deck


class CollectionVerseSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('reference', 'source', 'topic', 'passage', 'image')
        model = Verse
